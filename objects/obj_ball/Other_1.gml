/// @description Insert description here
// You can write your code in this editor

if (bbox_left < 0 || bbox_right > room_width) {
	hspeed *= -1;
} else if (bbox_top < 0) {
	vspeed *= -1;
} else if (bbox_bottom > room_height) {
	global.player_lives -= 1;
	instance_destroy();
	if (global.player_lives <= 0) {
		if (global.player_score > global.high_score) {
			global.high_score = global.player_score;
		}
		obj_control.gameover = true;
	} else {
		instance_create_layer(xstart, ystart, "Instances", obj_ball);
	}
}
