/// @description Insert description here
// You can write your code in this editor

draw_text(5, 5, "Score: " + string(global.player_score));
draw_set_halign(fa_right);
draw_text(room_width - 5, 5, "High Score: " + string(global.high_score));
if (gameover) {
	draw_set_font(fnt_text_large);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_text(room_width / 2, obj_bat.y - 30, "Game Over");
	draw_set_valign(fa_left);
	draw_set_font(fnt_text);
}
draw_set_halign(fa_left);

life_shift = 64
life_x = (room_width / 2) - ((global.player_lives - 1) * (life_shift / 2));
repeat (global.player_lives) {
	draw_sprite_ext(spr_bat, 0, life_x, room_height - 16, 0.75, 0.75, 0, c_white, 0.5);
	life_x += 64
}
