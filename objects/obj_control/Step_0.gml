/// @description Insert description here
// You can write your code in this editor

if (instance_number(obj_brick) <= 0) {
	room_restart();
}

if (gameover && keyboard_check_pressed(vk_anykey)) {
	room_restart();
	global.player_lives = 3;
	global.player_score = 0;
}
